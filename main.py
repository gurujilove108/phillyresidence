#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask, request, jsonify
import jinja2
import os
import logging

# flask app and jinja2 settings


from blueprints.index_blueprint import index_blueprint
from blueprints.dao_blueprint import dao_blueprint


def create_flask_app():
    app = Flask(__name__)
    template_dir = os.path.join(
        os.path.dirname(__file__),
        'templates')
    jinja_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(template_dir),
        autoescape=True)
    app.jinja_env = jinja_env
    app.debug = True
    return app


def register_blueprints(app):
    for blueprint in [index_blueprint, dao_blueprint]:
        app.register_blueprint(blueprint)


app = create_flask_app()
register_blueprints(app)



















