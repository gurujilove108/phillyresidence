import logging
import time
import json

from flask import Blueprint, jsonify, request, render_template, redirect, url_for, make_response

import flask

from datastore_models.property_models.bright_buso import BrightBusoListing
from datastore_models.property_models.bright_coms import BrightComsListing
from datastore_models.property_models.bright_farm import BrightFarmListing
from datastore_models.property_models.bright_land import BrightLandListing
from datastore_models.property_models.bright_coml import BrightComlListing
from datastore_models.property_models.bright_listing import BrightListing
from datastore_models.property_models.bright_resi import BrightResiListing
from datastore_models.property_models.bright_rinc import BrightRincListing
from datastore_models.property_models.bright_rlse import BrightRlseListing

from gaesessions import get_current_session
dao_blueprint = Blueprint('dao_blueprint', __name__, template_folder='templates')

num_results = 50

# property sub type routes for all properties
@dao_blueprint.route("/upload_bright_land", methods=["post"])
def upload_bright_land_json():
    try:
        d = request.get_json(force=True)
        BrightLandListing.store_or_update(d)
        logging.info("\n\n")
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_coml", methods=["post"])
def upload_bright_coml_json():
    try:
        j = request.get_json(force=True)
        BrightComlListing.store_or_update(j)
        
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_coms", methods=["post"])
def upload_bright_coms_json():
    try:
        j = request.get_json(force=True)
        BrightComsListing.store_or_update(j)
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_resi", methods=["post"])
def upload_bright_resi_json():
    try:
        j = request.get_json(force=True)
        BrightResiListing.store_or_update(j)
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_rlse", methods=["post"])
def upload_bright_rlse_json():
    try:
        j = request.get_json(force=True)
        BrightRlseListing.store_or_update(j)
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_rinc", methods=["post"])
def upload_bright_rinc_json():
    try:
        j = request.get_json(force=True)
        BrightRincListing.store_or_update(j)
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"

@dao_blueprint.route("/upload_bright_farm", methods=["post"])
def upload_bright_farm_json():
    try:
        j = request.get_json(force=True)
        BrightFarmListing.store_or_update(j)
        return "success"
    except Exception as e:
        logging.info(str(e))
        return "failure"


@dao_blueprint.route("/upload_bright_buso", methods=["post"])
def upload_bright_buso_json():
    try:
        j = request.get_json(force=True)
        BrightBusoListing.store_or_update(j)
        return "success"

    except Exception as e:
        logging.info(str(e))
        return "failure"

@dao_blueprint.route("/api/listings/land")
def fetch_land_listings():
    try:

        landlistings = BrightLandListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])

@dao_blueprint.route("/api/listings/coml")
def fetch_coml_listings():
    try:
        landlistings = BrightComlListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/coms")
def fetch_coms_listings():
    try:
        landlistings = BrightComsListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/buso")
def fetch_buso_listings():
    try:
        landlistings = BrightBusoListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/resi")
def fetch_resi_listings():
    try:
        landlistings = BrightResiListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/rlse")
def fetch_rlse_listings():
    try:
        landlistings = BrightRlseListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/farm")
def fetch_farm_listings():
    try:
        landlistings = BrightFarmListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])


@dao_blueprint.route("/api/listings/rinc")
def fetch_rinc_listings():
    try:
        landlistings = BrightRincListing.query().fetch(num_results)
        listings_json = create_listing_json(landlistings)
        return flask.Response(listings_json, headers={'Access-Control-Allow-Origin': '*'})

    except Exception as e:
        logging.info(str(e))
        return json.dumps([])

property_type_mapping = {

}

@dao_blueprint.route("/search", methods=["post"])
def search():
    try:
        property_type = request.form["property_type"]
        logging.info(property_type)

    except Exception as e:
        logging.info(str(e))

def create_listing_json(listings):
    objects = []
    for obj in listings:
        obj_dict = obj.to_dict()
        for key in obj_dict.keys():
            if 'date' in key.lower():
                obj_dict[key] = str(obj_dict[key])
        objects.append(obj_dict)
    return json.dumps(objects)


