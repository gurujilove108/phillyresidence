const landListingsUrl = "https://phillyresidence.appspot.com/api/listings/land";
const comlListingsUrl = "https://phillyresidence.appspot.com/api/listings/coml";
const comsListingsUrl = "https://phillyresidence.appspot.com/api/listings/coms";
const busoListingsUrl = "https://phillyresidence.appspot.com/api/listings/buso";
const resiListingsUrl = "https://phillyresidence.appspot.com/api/listings/resi";
const rlseListingsUrl = "https://phillyresidence.appspot.com/api/listings/rlse";
const farmListingsUrl = "https://phillyresidence.appspot.com/api/listings/farm";
const rincListingsUrl = "https://phillyresidence.appspot.com/api/listings/rinc";

const searchUrl = "http://localhost:8080/search";


    // noinspection JSUnusedGlobalSymbols
    let indexViewModel = {

        current_listings: ko.observableArray(),
        current_title: ko.observable("Land For Sale"),
        current_view: ko.observable(),
        map_markers: ko.observableArray(),

        findMarker: (lat, lng) => {
            var markers = indexViewModel.map_markers();
            for (i = 0; i < markers.length; i++) {

                if (parseFloat(markers[i].position.lat().toString()).toFixed(4) === lat && parseFloat(markers[i].position.lng().toString()).toFixed(4) === lng) {
                    return markers[i];

                }
            }
        },

        propertyClicked: (obj) => {
            var markerMatch = indexViewModel.findMarker(parseFloat(obj.Latitude).toFixed(4), parseFloat(obj.Longitude).toFixed(4));
            markerMatch.setAnimation(google.maps.Animation.BOUNCE);

            setTimeout(function () {
                markerMatch.setAnimation(null);
            }, 3000);
            // infoWindow.setContent(markerMatch.title);
            // infoWindow.open(map, markerMatch);
        },

        createMapMarker: (obj) => {
            var lat = parseFloat(obj.Latitude);
            var lng = parseFloat(obj.Longitude);
            var marker = new google.maps.Marker({
                position: {lat: lat, lng: lng},
                // title: place.formatted_address,
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP
            });
            return marker;
        },

        add_listing: (obj) => {
            indexViewModel.current_listings.push(obj);
            indexViewModel.map_markers.push(indexViewModel.createMapMarker(obj));
        },

        initialize: () => {
            $.get(landListingsUrl, (data) => {
                indexViewModel.current_listings.removeAll()
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        setCurrentView: (obj) => {
            console.log(obj);
            indexViewModel.current_view(obj);
        },

        loadLand: () => {
            $.get(landListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();

                indexViewModel.current_listings.removeAll()
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadComl: () => {
            $.get(comlListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadComs: () => {
            $.get(comsListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadBuso: () => {
            $.get(busoListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadResi: () => {
            $.get(resiListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadRlse: () => {
            $.get(rlseListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadFarm: () => {
            $.get(farmListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        loadRinc: () => {
            $.get(rincListingsUrl, (data) => {
                indexViewModel.removeMapMarkers();
                indexViewModel.current_listings.removeAll();
                let json_obj = $.parseJSON(data);
                json_obj.forEach(obj => indexViewModel.add_listing(obj))
            })
        },

        removeMapMarkers: () => {
            var markers = indexViewModel.map_markers();
            for (i = 0; i < markers.length; i++) {
                markers[i].setMap(null);

            }
        },

        search: () => {
            let propertyType = jQuery("#property-selected").text();
            let searchKeyword = jQuery("#keyword").val().trim();

            indexViewModel.current_title(propertyType);

            if (propertyType !== "All" && searchKeyword === "") {
                if (propertyType === "Land") {
                    indexViewModel.loadLand();
                } else if (propertyType === "Commercial Lease") {
                    indexViewModel.loadComl();
                } else if (propertyType === "Commercial Sale") {
                    indexViewModel.loadComs();
                } else if (propertyType === "Business Opportunity") {
                    indexViewModel.loadBuso();
                } else if (propertyType === "Residential") {
                    indexViewModel.loadResi();
                } else if (propertyType === "Residential Lease") {
                    indexViewModel.loadRlse();
                } else if (propertyType === "Farm") {
                    indexViewModel.loadFarm();
                } else if (propertyType === "RINC") {
                    indexViewModel.loadRinc();
                }
            }
        },

        toggleSearch: () => {
            jQuery("#search-menu").toggleClass("open");
            if (jQuery("#search-menu").hasClass("open")) {
                jQuery("#main-container").css("margin-left", "300px");
            } else {
                jQuery("#main-container").css("margin-left", "0");
            }
        }
}
