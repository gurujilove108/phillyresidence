import logging

from google.appengine.ext import ndb
from dateutil import parser


class BrightListing(ndb.Expando):
    ListOfficeKey = ndb.StringProperty()
    LotSizeUnits = ndb.StringProperty()
    ListAgentPreferredPhone = ndb.StringProperty()
    SchoolDistrictKey = ndb.StringProperty()
    MLSListDate = ndb.DateTimeProperty()
    Directions = ndb.StringProperty()
    Longitude = ndb.StringProperty()
    PricePerSquareFoot = ndb.StringProperty()
    SyndicationRemarks = ndb.StringProperty()
    LostSizeAcres = ndb.StringProperty()
    StreetName = ndb.StringProperty()
    ListAgentMlsId = ndb.StringProperty()
    Latitude = ndb.StringProperty()
    AssessmentYear = ndb.StringProperty()
    LotSizeArea = ndb.StringProperty()
    PublicRemarks = ndb.StringProperty()
    FullStreetAddress = ndb.StringProperty()
    TaxYear = ndb.StringProperty()
    ListingTaxID = ndb.StringProperty()
    ListAgentFullName = ndb.StringProperty()
    ListAgentEmail = ndb.StringProperty()
    ListAgentKey = ndb.StringProperty()
    LotSizeSquareFeet = ndb.StringProperty()
    PostalCode = ndb.StringProperty()
    UnparsedAddress = ndb.StringProperty()
    ListingKey = ndb.StringProperty()
    TaxAnnualAmount = ndb.StringProperty()
    CloseDate = ndb.DateTimeProperty()
    ListPrice = ndb.StringProperty()
    SubdivisionName = ndb.StringProperty()
    MlsStatus = ndb.StringProperty()
    ListingId = ndb.StringProperty()
    City = ndb.StringProperty()
    GeoPoint = ndb.GeoPtProperty()

    
    def update(self, new_listing):
        for key in new_listing.keys():
            key_lower = key.lower()
            if 'date' in key_lower:
                if new_listing[key]:
                    dt = parser.parse(new_listing[key])
                    setattr(self, key, dt)
                else:
                    setattr(self, key, None)

            elif not new_listing[key]:
                setattr(self, key, None)

            else:
                setattr(self, key, new_listing[key])

        try:
            pass
        except Exception as e:
            pass

        return self

