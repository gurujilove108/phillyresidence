import logging

from google.appengine.ext import ndb

from datastore_models.property_models.bright_listing import BrightListing
from dateutil import parser


class BrightComlListing(BrightListing):

    @classmethod
    def store_or_update(cls, obj):
        exists = cls.query(cls.ListingId == obj["ListingId"]).get()
        if not exists:
            new_listing = cls()
            new_listing = new_listing.update(obj)
            new_listing.put()
            logging.info("new listing stored")

        else:
            exists = exists.update(obj)
            exists.put()
            logging.info("listing updated")